const ArgLib = require('arg');
const Lodash = require('lodash');

const _getProvider = {
    arg: (vars, settings) => {
        let argLibConfig = {};
        let argConfig = {};
        for(let key in vars) {
            argLibConfig['--' + key.split('.').join('-')] = vars[key].type
        }
        if(settings.helper && !argLibConfig['--help']) {
            argLibConfig['--help'] = Boolean;
            argLibConfig['-h'] = Boolean;
        }
        let commandArguments = ArgLib(argLibConfig, options = {permissive: true});
        for(let key in vars) {
            let commandArgumentsKey = '--' + key.split('.').join('-');
            if(commandArguments[commandArgumentsKey]) {
                argConfig[key] = commandArguments[commandArgumentsKey]
            }
        }
        return argConfig;
    },
    env: (vars, settings) => {
        let prefix = '';
        let envConfig = {};
        if(settings.prefix) {
            prefix = settings.prefix.toUpperCase() + '_';
        }
        for(let key in vars) {
            let keyEnv = key.split('.').join('_').toUpperCase()
            if(process.env[prefix + keyEnv]) {
                envConfig[key] = new vars[key].type(process.env[prefix + keyEnv])
            }
        }
        return envConfig;
    },
    default: (vars, settings) => {
        let defaultConfig = {};
        for(let key in vars) {
            if(vars[key].default) {
                defaultConfig[key] = vars[key].default;
            }
        }
        return defaultConfig;
    }
}

function ArgConf(context) {

    let argProviders = {}

    for(let provider in context.providers) {
        argProviders[context.providers[provider].priority] = _getProvider[provider](context.vars, context.providers[provider].settings);
    }

    let argProvidersArray = Object.values(argProviders);
    argProvidersArray.unshift(_getProvider.default(context.vars, {}));

    let argProvidersMerged = argProvidersArray[0];

    for(let i = 1;i < argProvidersArray.length; i++) {
        argProvidersMerged = Lodash.merge(argProvidersMerged, argProvidersArray[i]);
    }

    for(let key in context.vars){
        if(context.vars[key].required && !argProvidersMerged[key]) {
            throw new Error(`[ArgConf] ${key} is required`);
        }
    }

    let argsObject = {};

    for (let key in argProvidersMerged) {
        Lodash.set(argsObject, key, argProvidersMerged[key])
    }

    return argsObject;

}

module.exports = ArgConf;

